## Channels

[Taking 20](https://www.youtube.com/channel/UCly0Thn_yZouwdJtg7Am62A)  
[WASD20](https://www.youtube.com/channel/UCQOmmyopiTzH9dlThm8hbwQ)  
[How to be a Great GM](https://www.youtube.com/channel/UC1F4eMw3W_rHBfxf9_m1hbw)  
[Rogue Watson](https://www.youtube.com/channel/UCRi-TNndIn_SohMHaMAUW0A)  
[Official D&D](https://www.youtube.com/user/DNDWizards/featured)  
[Official Roll20](https://www.youtube.com/user/MsRoll20)  


## Videos

[Ranking ALL the Dungeons and Dragons 5e Adventures Worst to Best](https://www.youtube.com/watch?v=_VIrIOMJovM)  
[The Easy way to a Backstory for your Character - PC Tips](https://www.youtube.com/watch?v=so2IHwVWZ-4)  

## Subreddits

[Roll 20](https://old.reddit.com/r/Roll20/)  
[D&D Related Subreddits](https://www.reddit.com/r/DnD/wiki/related_subreddits)  
[D100 - Random Lists of Shit](https://www.reddit.com/r/d100/)  
[DM Academy](https://old.reddit.com/r/DMAcademy/)  
[PC Academy](https://old.reddit.com/r/PCAcademy/)  
[Behind the Screen - DM Tools and resources](https://old.reddit.com/r/DnDBehindTheScreen/)  
[D&D Next - 5th Edition Discussions](https://old.reddit.com/r/dndnext/)  

[Forgotten Realms](https://old.reddit.com/r/Forgotten_Realms/)  

## Links

[Reddit D&D Wiki](https://www.reddit.com/r/DnD/wiki/index?utm_source=reddit&utm_medium=usertext&utm_name=DnD&utm_content=t5_2r9ei)  
[Random Town Generator](https://www.reddit.com/r/DnD/comments/5vjah6/my_excel_sheet_for_randomly_generating_almost/?utm_source=reddit&utm_medium=usertext&utm_name=DnD&utm_content=t5_2r9ei)  
[5e Wiki for Spells and Class Features](http://dnd5e.wikidot.com/)  



So basically like once you decide your race and what class you want to play and all of that, then you just need to come up with some sort of back story. Like why your character is doing what they are doing. For example if you're a fighter type, maybe your story is that you got bored just manning a guard post somewhere and wanted something more exciting (and it turns out that you are actually a deserter on the run). Or maybe you're a monk and you got kicked out of your order for breaking your vow of silence