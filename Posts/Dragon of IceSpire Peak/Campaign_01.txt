__**Storm King's Thunder**__

Ages ago, giants and dragons waged war across the Savage Frontier. These battles are long forgotten by the human civilizations of today, but ancient relics remain. And now, the land shudders once more with the thunder of giant footsteps.

Puny adventurers must rise to the challenge, gather their strength, unlock the power of ancient runes, and take the fight to the giants’ doorsteps. Only then can they forge an alliance to end the war before it begins.


**__Dragon Heist**__

Famed explorer Volothamp Geddarm needs you to complete a simple quest. Thus begins a mad romp through the wards of Waterdeep as you uncover a villainous plot involving some of the city’s most influential figures.

A grand urban caper awaits you. Pit your skill and bravado against villains the likes of which you’ve never faced before, and let the dragon hunt begin!


__**Tomb of Annihilation**__

The talk of the streets and taverns has all been about the so-called death curse: a wasting disease afflicting everyone who’s ever been raised from the dead. Victims grow thinner and weaker each day, slowly but steadily sliding toward the death they once denied.

When they finally succumb, they can’t be raised — and neither can anyone else, regardless of whether they’ve ever received that miracle in the past. The cause is a necromantic artifact called the Soulmonger, which is located somewhere in Chult, a mysterious peninsula far to the south, ringed with mountains and choked with rainforests.


__**Baldur's Gate: Descent into Avernus**__

Welcome to Baldur’s Gate, a city of ambition and corruption situated at the crossroads of the Sword Coast. You’ve just started your adventuring career, but already find yourself embroiled in a plot that sprawls from the shadows of Baldur’s Gate to the front lines of the planes-spanning Blood War! Do you have what it takes to turn infernal war machines and nefarious contracts against the archdevil Zariel and her diabolical hordes? And can you ever hope to find your way home safely when pitted against the infinite evils of the Nine Hells?


__**Curse of Strahd**__

Under raging storm clouds, the vampire Count Strahd von Zarovich stands silhouetted against the ancient walls Castle Ravenloft. Rumbling thunder pounds the castle spires. The wind’s howling increases as he turns his gaze down toward the village of Barovia.

Far below, yet not beyond his keen eyesight, a party of adventurers has just entered his domain. Strahd’s face forms the barest hint of a smile as his dark plan unfolds. He knew they were coming, and he knows why they came - all according to his plan.

A lightning flash rips through the darkness, but Strahd is gone. Only the howling of the wind fills the midnight air. The master of Castle Ravenloft is having guests for dinner. And you are invited.


__**TL;DR**__

**Storm King's Thunder** - Giants.
**Dragon Heist** - Caper adventure in the city of Waterdeep.
**Tomb of Annihilation** - Forests and dinosaurs.
**Baldur's Gate: Descent into Avernus** - We're going to hell.
**Curse of Strahd** - Gothic horror. Vampires. Werewolves. Undead.