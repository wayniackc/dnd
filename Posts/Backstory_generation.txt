Here is a video that explains a good process for creating a backstory for your character:

https://www.youtube.com/watch?v=so2IHwVWZ-4

And here is a good comment from that video as well with a slightly different method that might also help with the process:

> I've mentioned this before, but my preferred method is what I like to call "deductive character generation".
> 
> I fill out a character sheet as completely as I can (usually have a basic concept I want to play like "fast duelist mage" or "quadroplegic psionic" to go with), then I sit and look at the stuff I put and ask "why" and/or "how" on everything that's unique about them: if he's stronger then normal, I ask why he's such a beefcake;  if she's skilled at climbing, I am how she learned to climb so we'll; etc. 
> 
> Whatever answers come out, I explore and try to link to any other answers I already have. If the answer is unsatisfactory ("I don't know why he's strong he just is") or it doesn't at all fit with the other answers ("She's an introverted wizard who hates sunlight because she's a Drow, but she spontaneously took a repelling class for a week"), then I evaluate whether it needs tweaking and try again as necessary.
> 
> Using this method, I usually have about 2-3 pages of backstory within about 20 minutes of work. I then try to pair it down to 1 page to make the GM's life easier and so the character has room to grow in the world and story.